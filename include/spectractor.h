#ifndef SPECTRACTOR_H
#define SPECTRACTOR_H

#include <QtCore/qglobal.h>

#include <string>
#include <utility>
#include <vector>
#include <QDialog>

#if defined(SPECTRACTOR_BUILD_DLL)
    #define SPECTRACTOR_API Q_DECL_EXPORT
#else
    #define SPECTRACTOR_API Q_DECL_IMPORT
#endif

namespace ECHMET {

class SPECTRACTOR_API Spectractor {
public:
    class Trace {
    public:
        enum Base {
            TIME,
            WAVELENGTH
        };

        using SVec = std::vector<std::tuple<double, double>>;

        explicit Trace();
        Trace(const Base _base, const double _value, const double _reference,
              std::string xName, std::string xUnit,
              std::string yName, std::string yUnit,
              SVec _vec) noexcept;
        Trace(const Trace &other) = default;
        Trace(Trace &&other) noexcept;

        Base base;
        double value;
        double reference;
        std::string xName;
        std::string xUnit;
        std::string yName;
        std::string yUnit;
        SVec data;
        bool valid;
    };

    class Dialog : public QDialog {
    public:
        explicit Dialog(QWidget *parent = nullptr);

        virtual Trace trace() = 0;
    };

    explicit Spectractor() = delete;

    static Dialog *proxyDialog(const std::string &path);
    static Dialog *standaloneDialog();
};

}

#endif // SPECTRACTOR_H
