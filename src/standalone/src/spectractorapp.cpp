#include <QApplication>
#include <spectractor.h>

#include <QFileDialog>

using namespace ECHMET;

int main(int argc, char **argv)
{
    QApplication app{argc, argv};

    app.setQuitOnLastWindowClosed(true);

    auto mWin = Spectractor::standaloneDialog();
    mWin->show();

    return app.exec();
}
