#ifndef GLOBALS_H
#define GLOBALS_H

#include <QString>

class Globals {
public:
    Globals() = delete;

    static const QString ORG_NAME;
    static const QString SOFTWARE_NAME;
    static const int VERSION_MAJ;
    static const int VERSION_MIN;
    static const QString VERSION_REV;
};

#endif // GLOBALS_H
