#include "globals.h"

const QString Globals::ORG_NAME{"ECHMET"};
const QString Globals::SOFTWARE_NAME{"Spectractor"};
const int Globals::VERSION_MAJ{0};
const int Globals::VERSION_MIN{0};
const QString Globals::VERSION_REV{"a"};
