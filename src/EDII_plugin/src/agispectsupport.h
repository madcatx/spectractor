#ifndef AGISPECTSUPPORT_H
#define AGISPECTSUPPORT_H

#include <QtCore/qglobal.h>

#if defined(AGISPECTSUPPORT_BUILD_DLL)
    #define AGISPECTSUPPORT_API Q_DECL_EXPORT
#else
    #define AGISPECTSUPPORT_API Q_DECL_IMPORT
#endif

#include <plugininterface.h>

class QStringList;

namespace plugin {

class LoadCsvFileDialog;

class AGISPECTSUPPORT_API AgiSpectSupport : public EDIIPlugin {
public:
    virtual Identifier identifier() const override;
    virtual void destroy() override;
    virtual std::vector<Data> load(const int option) override;
    virtual std::vector<Data> loadHint(const std::string &hintPath, const int option) override;
    virtual std::vector<Data> loadPath(const std::string &path, const int option) override;

    static AgiSpectSupport *instance(UIPlugin *plugin);

private:
    AgiSpectSupport(UIPlugin *plugin);
    virtual ~AgiSpectSupport() override = default;
    std::vector<Data> loadInteractive(const std::string &path);
    std::vector<Data> loadFromFile(const std::string &path);

    UIPlugin *m_uiPlugin;

    static AgiSpectSupport *s_me;
    static const Identifier s_identifier;
};

extern "C" {
    AGISPECTSUPPORT_API EDIIPlugin * initialize(UIPlugin *plugin);
}

} // namespace plugin

#endif // AGISPECTSUPPORT_H
