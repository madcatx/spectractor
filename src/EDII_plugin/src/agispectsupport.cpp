#include "agispectsupport.h"

#include <pluginhelpers_p.h>
#include <threadeddialog.h>
#include <sstream>
#include <QFileDialog>
#include <spectractor.h>
#include <cassert>

namespace plugin {

class OpenFileThreadedDialog : public ThreadedDialog<QFileDialog> {
public:
    OpenFileThreadedDialog(UIPlugin *plugin, QString path) :
        ThreadedDialog<QFileDialog>{
            plugin,
            [this]() {
                auto dlg = new QFileDialog{nullptr, QObject::tr("Pick a text file"), this->m_path};

                dlg->setAcceptMode(QFileDialog::AcceptOpen);
                dlg->setFileMode(QFileDialog::ExistingFile);
                dlg->setNameFilters({
                                      QObject::tr("UV spectrum file(*.UV *.uv)"),
                                      QObject::tr("All Files (*)")
                                    });

                return dlg;
            }
       },
       m_path(std::move(path))
    {}

private:
    const QString m_path;
};

class SpectractorThreadedDialog : public ThreadedDialog<ECHMET::Spectractor::Dialog> {
public:
    SpectractorThreadedDialog(UIPlugin *plugin, std::string path) :
        ThreadedDialog<ECHMET::Spectractor::Dialog>{
            plugin,
            [this]() {
                return ECHMET::Spectractor::proxyDialog(this->m_path);
            }
        },
        m_path(std::move(path))
    {}

private:
    const std::string m_path;
};

AgiSpectSupport *AgiSpectSupport::s_me{nullptr};
const Identifier AgiSpectSupport::s_identifier{"Agilent UV file format support", "Agilent UV", "AGUV", {}};

AgiSpectSupport::AgiSpectSupport(UIPlugin *plugin) :
    m_uiPlugin{plugin}
{}

void AgiSpectSupport::destroy()
{
    delete s_me;
}

Identifier AgiSpectSupport::identifier() const
{
    return s_identifier;
}

AgiSpectSupport * AgiSpectSupport::instance(UIPlugin *plugin)
{
    if (s_me == nullptr) {
        try {
            s_me = new AgiSpectSupport{plugin};
        } catch (const std::bad_alloc &) {
            s_me = nullptr;
        }
    }

    return s_me;
}

std::vector<Data> AgiSpectSupport::load(const int)
{
    return loadInteractive("");
}

std::vector<Data> AgiSpectSupport::loadHint(const std::string &hintPath, const int)
{
    return loadInteractive(hintPath);
}

std::vector<Data> AgiSpectSupport::loadFromFile(const std::string &path)
{
    SpectractorThreadedDialog spectDlgWrap{m_uiPlugin, path};
    if (spectDlgWrap.execute() != QDialog::Accepted)
        return {};

    auto trace = spectDlgWrap.dialog()->trace();
    if (!trace.valid)
        return {};

    std::string id = [](const ECHMET::Spectractor::Trace::Base b, const double value, const double reference) {
        if (b == ECHMET::Spectractor::Trace::TIME) {
            const std::string NM{" (nm)"};
            std::ostringstream oss{};
            oss.precision(1);
            oss << std::fixed << value / 1000.0;

            auto ret = std::string{"wavelength = "} + oss.str() + NM;

	    if (reference >= 0.0) {
		    oss.str("");
		    oss << std::fixed << reference / 1000.0;

		    ret += std::string{", reference = "} + oss.str() + NM;
	    }
	    return ret;
        } else {
            std::ostringstream oss{};
            oss.precision(4);
            oss << std::fixed << value;

            return std::string{"time = "} + oss.str() + std::string{" (min)"};
        }
        assert(false);
    }(trace.base, trace.value, trace.reference);

    std::string name = QFileInfo{QString::fromStdString(path)}.fileName().toStdString();

    Data d {
        std::move(name),
        std::move(id),
        path,
        std::move(trace.xName),
        std::move(trace.yName),
        std::move(trace.xUnit),
        std::move(trace.yUnit),
        std::move(trace.data)};

    return {d};
}

std::vector<Data> AgiSpectSupport::loadInteractive(const std::string &path)
{
    OpenFileThreadedDialog dlgWrap{m_uiPlugin, QString::fromStdString(path)};

    if (dlgWrap.execute() != QDialog::Accepted)
        return {};

    const auto files = dlgWrap.dialog()->selectedFiles();
    if (files.empty())
        return {};

    return loadFromFile(files[0].toStdString());
}

std::vector<Data> AgiSpectSupport::loadPath(const std::string &path, const int)
{
    return loadFromFile(path);
}

EDIIPlugin * initialize(UIPlugin *plugin)
{
    return AgiSpectSupport::instance(plugin);
}

EDIIPlugin::~EDIIPlugin()
{}

} // plugin
