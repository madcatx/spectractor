#include "readworker.h"

#include <slepitzspect.h>

class LSPTZTraceReleaser {
public:
    explicit LSPTZTraceReleaser(LSPTZ_Trace &trace) :
        h_trace{trace}
    {}
    LSPTZTraceReleaser(const LSPTZTraceReleaser &) = delete;
    LSPTZTraceReleaser(LSPTZTraceReleaser &&) = delete;
    LSPTZTraceReleaser & operator=(const LSPTZTraceReleaser &) = delete;
    LSPTZTraceReleaser & operator=(LSPTZTraceReleaser &&) = delete;

    ~LSPTZTraceReleaser() {
        lsptz_free(&h_trace);
    }

private:
    LSPTZ_Trace &h_trace;
};

InvalidFileError::InvalidFileError() :
    std::runtime_error{"File does not appear to be a valid UV file"}
{}

NoDataError::NoDataError() :
    std::runtime_error{"No data was loaded"}
{}

ReadWorker::ReadWorker(std::string path) noexcept :
    ok{false},
    ex{"No error"},
    m_path(std::move(path))
{}

void ReadWorker::process()
{
    try {
        spgram = read(m_path);
        ok = true;
    } catch (const std::runtime_error &_ex) {
        ok = false;
        ex = _ex;
    }

    emit finished();
}

SpectractorDialog::SPGram ReadWorker::read(const std::string &path)
{
    LSPTZ_Trace trace;
    LSPTZ_ReadErrors error;

    auto ret = lsptz_read(path.c_str(), &trace, &error);

    if (ret != LSPTZ_Success)
        throw InvalidFileError{};

    LSPTZTraceReleaser releaser{trace};

    uint32_t wlFrom;
    uint32_t wlTo;
    float timeFrom;
    float timeTo;
    float absMin = std::numeric_limits<float>::max();
    float absMax = std::numeric_limits<float>::min();
    QVector<double> datapoints{};
    int columns;
    uintptr_t rows;
    uint32_t wlStep = 0;

    if (trace.n_spectra < 1)
        throw InvalidDataError{"File contains no data"};

    /* First pass to get data ranges.
     * We expect the data ranges to be the same for all remaining traces */
    {
        auto spectrum = &trace.spectra[0];

        if (spectrum->n_points < 1)
            throw InvalidDataError{QString{tr("Time segment contains no spectral data")}.toUtf8()};
        rows = spectrum->n_points;

        wlFrom = spectrum->spectrum[0].wavelength;
        wlTo = spectrum->spectrum[spectrum->n_points - 1].wavelength;

        if (spectrum->n_points > 1)
            wlStep = spectrum->spectrum[1].wavelength - wlFrom;

        if (trace.n_spectra > uintptr_t(std::numeric_limits<int>::max()))
            throw InvalidDataError{QString{tr("Data contains too many time points and is too large to display")}.toUtf8()};

        columns = int(trace.n_spectra);

        timeFrom = spectrum->time;
        timeTo = trace.spectra[trace.n_spectra - 1].time;
    }

    for (uintptr_t row = 0; row < rows; row++) {
        for (int col = 0; col < columns; col++) {
            auto spectrum = &trace.spectra[col];

            if (rows != spectrum->n_points)
                throw InvalidDataError{"Mismatching nubmer of columns"};

            if (spectrum->spectrum[0].wavelength != wlFrom)
                throw InvalidDataError{QString{tr("Mismatching value of start wavelength")}.toUtf8()};
            if (spectrum->spectrum[spectrum->n_points - 1].wavelength != wlTo)
                throw InvalidDataError{QString{tr("Mismatching value of end wavelength")}.toUtf8()};

            const auto a = spectrum->spectrum[row].absorbance;

            if (a < absMin)
                absMin = a;
            if (a > absMax)
                absMax = a;

            datapoints.append(double(a));
        }
    }

    /* Make matrix mapping */
    SpectractorDialog::TimeColMap tcMap{};
    for (uintptr_t col = 0; col < trace.n_spectra; col++) {
        auto spectrum = &trace.spectra[col];

        tcMap.emplace_back(spectrum->time, int(col));
    }
    SpectractorDialog::WlRowMap wlrMap{};
    {
        auto spectrum = &trace.spectra[0];
        for (uintptr_t row = 0; row < rows; row++) {
            auto spt = &spectrum->spectrum[row];

            wlrMap.emplace(spt->wavelength, row);
        }
    }

    QwtInterval timeInterval{double(timeFrom), double(timeTo)};
    QwtInterval wlInterval{double(wlFrom), double(wlTo)};
    QwtInterval absInterval{double(absMin), double(absMax)};

    return {timeInterval, wlInterval, absInterval, std::move(datapoints), columns, QString{trace.metadata.units},
            std::move(tcMap), std::move(wlrMap), wlStep};

}
