#ifndef READWORKER_H
#define READWORKER_H

#include <QObject>
#include "ui/spectractordialog.h"

class BadModeError : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

class InvalidDataError : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

class InvalidFileError : public std::runtime_error {
public:
    explicit InvalidFileError();
};

class NoDataError : public std::runtime_error {
public:
    explicit NoDataError();
};

class ReadWorker : public QObject {
    Q_OBJECT
public:
    explicit ReadWorker(std::string path) noexcept;

    bool ok;
    SpectractorDialog::SPGram spgram;
    std::runtime_error ex;

public slots:
    void process();

signals:
    void finished();

private:
    SpectractorDialog::SPGram read(const std::string &path);

    const std::string m_path;
};

#endif // READWORKER_H
