#ifndef REFERENCEWAVELENGTHWIDGET_H
#define REFERENCEWAVELENGTHWIDGET_H

#include <QWidget>

namespace Ui {
class ReferenceWavelengthWidget;
}

class ReferenceWavelengthWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ReferenceWavelengthWidget(QWidget *parent = nullptr);
    ~ReferenceWavelengthWidget();

    double wavelength() const;
    bool referenceEnabled() const;
    void setRange(const double min, const double max, const double step);

private:
    Ui::ReferenceWavelengthWidget *ui;

signals:
    void stateChanged(const bool enabled, const double wavelength);

};

#endif // REFERENCEWAVELENGTHWIDGET_H
