#ifndef QWTSPECTROGRAMPICKER_H
#define QWTSPECTROGRAMPICKER_H

#include <qwt_plot_picker.h>
#include <QColor>
#include <QLocale>

class QwtRasterData;

class QwtSpectrogramPicker : public QwtPlotPicker {
    Q_OBJECT
public:
	QwtSpectrogramPicker(QWidget *canvas);
	QwtSpectrogramPicker(int xAxis, int yAxis, RubberBand rubberBand, DisplayMode trackerMode, QWidget *canvas);
	~QwtSpectrogramPicker() override = default;

    void setColor(const QColor &color);
    void setRasterData(const QwtRasterData *data);

protected:
    QwtText trackerText(const QPoint &pos) const override;
    QwtText trackerTextF(const QPointF &posF) const override;

private:
    const QwtRasterData *m_data;
    const QLocale m_loc;
    const QChar m_sep;
    QColor m_color;

};

#endif // QWTSPECTROGRAMPICKER_H
