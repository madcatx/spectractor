#include "spectractordialog.h"
#include "ui_spectractordialog.h"
#include "../globals.h"
#include "sliceview.h"
#include "spectrumview.h"
#include "../exporter.h"
#include "loadinprogressdialog.h"
#include "../readworker.h"

#include <cmath>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSplitter>
#include <QThread>
#include <QTimer>
#include <QVBoxLayout>

#include <cassert>

SpectractorDialog::SelectedSlice::SelectedSlice() :
    base{TIME},
    row{0},
    col{0},
    time{0},
    wavelength{0},
    reference{-1, -1},
    data{QVector<QPointF>{}},
    valid{false}
{}

SpectractorDialog::SelectedSlice::SelectedSlice(const Base _base, const int _row, const int _col,
                                                const double _time, const int _wavelength, std::pair<int, int> _reference,
                                                const QwtInterval &_yInterval,
                                                QVector<QPointF> _data) :
    base{_base},
    row{_row},
    col{_col},
    time{_time},
    wavelength{_wavelength},
    reference(std::move(_reference)),
    yInterval{_yInterval},
    data(std::move(_data)),
    valid{true}
{}

SpectractorDialog::SelectedSlice::SelectedSlice(SelectedSlice &&other) noexcept :
    base{other.base},
    row{other.row},
    col{other.col},
    time{other.time},
    wavelength{other.wavelength},
    reference(std::move(other.reference)),
    yInterval(other.yInterval),
    data(std::move(other.data)),
    valid{other.valid}
{}

SpectractorDialog::SelectedSlice & SpectractorDialog::SelectedSlice::operator=(SelectedSlice &&other) noexcept
{
    base = other.base;
    row = other.row;
    col = other.col;
    time = other.time;
    wavelength = other.wavelength;
    reference = std::move(other.reference);
    yInterval = other.yInterval;
    data = std::move(other.data);
    valid = other.valid;

    return *this;
}

SpectractorDialog::SpectractorDialog(QWidget *parent) :
    ECHMET::Spectractor::Dialog{parent},
    ui{new Ui::SpectractorDialog},
    m_spectrumMatrixCols{0},
    m_mode{STANDALONE}
{
    ui->setupUi(this);
    m_mainMenu = makeMenuStandalone();

    m_fileDlg = new QFileDialog{this, tr("Select UV spectrum file")};
    m_fileDlg->setFileMode(QFileDialog::ExistingFile);
    m_fileDlg->setNameFilters({
                                tr("UV spectrum file(*.UV *.uv)"),
                                tr("All Files (*)")
                              });

    m_exportFileDlg = new QFileDialog{this, tr("Save spectrum slice")};
    m_exportFileDlg->setNameFilters({ tr("CSV file(*.csv)") });
    m_exportFileDlg->setAcceptMode(QFileDialog::AcceptSave);

    setupWidgetCommon();

    makeMenuStandalone();

    setWindowFlags(Qt::Window | Qt::WindowMinimizeButtonHint |
		   Qt::WindowMaximizeButtonHint |
		   Qt::WindowCloseButtonHint);
}

SpectractorDialog::SpectractorDialog(const QString &path, QWidget *parent) :
    ECHMET::Spectractor::Dialog{parent},
    ui{new Ui::SpectractorDialog},
    m_fileDlg{nullptr},
    m_spectrumMatrixCols{0},
    m_exportFileDlg{nullptr},
    m_mode{PROXY}
{
    ui->setupUi(this);
    m_mainMenu = makeMenuProxy();

    setupWidgetCommon();

    if (!loadSpectrum(path))
        QTimer::singleShot(0, this, [this]() { this->close(); });
}

SpectractorDialog::~SpectractorDialog()
{
    delete ui;
}

void SpectractorDialog::displaySlice(const SelectedSlice &slice)
{
    if (!slice.valid)
        return;

    const auto xTitle = slice.base == SelectedSlice::TIME ? "time (min)" : QString{"\xCE\xBB (nm)"};
    const auto baseTitle = slice.base == SelectedSlice::TIME ? QString{"\xCE\xBB = %1 (nm)"}.arg(double(slice.wavelength) / 1000.0) :
                                                               QString{"t = %1 (min)"}.arg(slice.time);

    m_sliceView->setPlotData(slice.data, xTitle, QString{"Absorbance (%1)"}.arg(m_absUnit),
                             baseTitle);
    m_spectrumView->setCrosshair(slice.time, double(slice.wavelength) / 1000.0);
}

QIcon SpectractorDialog::getIcon(const IconID id) const
{
#ifdef Q_OS_UNIX
	switch (id) {
	case IconID::LOAD:
		return QIcon::fromTheme("document-open");
	case IconID::EXIT:
		return QIcon::fromTheme("application-exit");
	}
	assert(false);
#elif defined(Q_OS_WIN)
	switch (id) {
	case IconID::LOAD:
		return style()->standardIcon(QStyle::SP_DialogOpenButton);
	case IconID::EXIT:
		return style()->standardIcon(QStyle::SP_DialogCloseButton);
	}
	assert(false);
#else
#endif // Q_OS_
}

bool SpectractorDialog::haveData() const
{
    return !(m_spectrumMatrix.empty() || m_spectrumMatrixCols < 1);
}

bool SpectractorDialog::loadSpectrum(const QString &path)
{
    try {
        auto spgram = readSpectrum(path);
        m_timeInterval = std::get<0>(spgram);
        m_wlInterval = std::get<1>(spgram);
        m_absInterval = std::get<2>(spgram);
        m_spectrumMatrix = std::move(std::get<3>(spgram));
        m_spectrumMatrixCols = std::get<4>(spgram);
        m_absUnit = std::move(std::get<5>(spgram));
        m_timeToColMapping = std::move(std::get<6>(spgram));
        m_wlToRowMapping = std::move(std::get<7>(spgram));

        const double wlMin = m_wlInterval.minValue() / 1000.0;
        const double wlMax = m_wlInterval.maxValue() / 1000.0;
        const double wlStep = double(std::get<8>(spgram)) /1000.0;

        m_sliceView->clear();
        m_selectedSlice = SelectedSlice();

        m_spectrumView->updateSpectrum(m_timeInterval,
                                       QwtInterval{wlMin, wlMax},
                                       m_absInterval,
                                       m_spectrumMatrix, m_spectrumMatrixCols, m_absUnit);
        m_spectrumView->setTitle(path);

        m_sliceView->wlRef()->setRange(wlMin, wlMax, wlStep);

        return true;
    } catch (const std::runtime_error &ex) {
        try {
            auto ife = dynamic_cast<const InvalidFileError &>(ex);
            QFileInfo finfo{path};

            QMessageBox mbox{QMessageBox::Warning, tr("Invalid file"),
                             QString{tr("File %1 you attempted to load does not appear to be a valid UV file")}.arg(finfo.baseName())};
            mbox.exec();
        } catch (std::bad_cast &) {
            QMessageBox mbox{QMessageBox::Warning, tr("Invalid data"),
                             QString{tr("File contains invalid data: %1")}.arg(ex.what())};
            mbox.exec();
        }
    }

    return false;
}

QMenu * SpectractorDialog::makeMenuProxy()
{
    return new QMenu{this};
}

QMenu * SpectractorDialog::makeMenuStandalone()
{
    auto m = new QMenu{tr("File"), this};

    m->addAction(getIcon(IconID::LOAD), tr("Load spectrum"), this, &SpectractorDialog::onLoadSpectrumInteractive);
    m->addSeparator();
    m->addAction(getIcon(IconID::EXIT), tr("Exit"), this, &SpectractorDialog::close);

    return m;
}

QVector<QPointF> SpectractorDialog::makeTimeSliceData(const int row, const int refRow)
{
    const double step = (m_timeInterval.maxValue() - m_timeInterval.minValue()) / m_spectrumMatrixCols;

    QVector<QPointF> vec{};
    double x = m_timeInterval.minValue();
    for (int col = 0; col < m_spectrumMatrixCols; col++) {
        const double y = m_spectrumMatrix.at(row * m_spectrumMatrixCols + col);
        vec.append(QPointF{x, y});
        x += step;
    }

    if (refRow >= 0) {
        for (int col = 0; col < m_spectrumMatrixCols; col++) {
            const double ry = m_spectrumMatrix.at(refRow * m_spectrumMatrixCols + col);
            auto &pt = vec[col];
            pt.setY(pt.y() - ry);
        }
    }

    return vec;
}

SpectractorDialog::SelectedSlice SpectractorDialog::makeSlice(const int row, const int col,
                                                              const double time, const int wavelength,
                                                              std::pair<int, int> reference,
                                                              const SelectedSlice::Base base)
{
    auto data = [&]() {
        switch (base) {
        case SelectedSlice::TIME:
            return makeTimeSliceData(row, reference.first);
        case SelectedSlice::WAVELENGTH:
            return makeWavelengthSliceData(col);
        }
        assert(false);
    }();

    double yMin = std::numeric_limits<double>::max();
    double yMax = std::numeric_limits<double>::min();

    for (const auto &pt : data) {
        const double y = pt.y();
        if (y < yMin)
            yMin = y;
        if (y > yMax)
            yMax = y;
    }


    return {base, row, col, time, wavelength, std::move(reference),
            QwtInterval{yMin, yMax}, std::move(data)};
}

QVector<QPointF> SpectractorDialog::makeWavelengthSliceData(const int col)
{
    const int rowLength = m_spectrumMatrix.size() / m_spectrumMatrixCols;
    const double step = (m_wlInterval.maxValue() - m_wlInterval.minValue()) / rowLength / 1000.0;

    QVector<QPointF> vec{};
    double x = m_wlInterval.minValue() / 1000.0;
    for (int row = 0; row < rowLength; row++) {
        const double y = m_spectrumMatrix.at(row * m_spectrumMatrixCols + col);
        vec.append(QPointF{x, y});
        x += step;
    }

    return vec;
}

std::pair<double, int> SpectractorDialog::mapTimeToColumn(const double t) const
{
    if (m_timeToColMapping.empty())
        throw std::runtime_error{"Empty mapping"};

    if (t < m_timeToColMapping.front().first)
        throw std::runtime_error{"Time out of range"};
    if (t > m_timeToColMapping.back().first)
        throw std::runtime_error{"Time out of range"};

    size_t leftIdx = 0;
    size_t rightIdx = m_timeToColMapping.size() - 1;

    while (rightIdx - leftIdx > 1) {
        const size_t tryIdx = (rightIdx - leftIdx) / 2 + leftIdx;

        const double tryT = m_timeToColMapping[tryIdx].first;

        if (t < tryT)
            rightIdx = tryIdx;
        else if (t > tryT)
            leftIdx = tryIdx;
        else {
            leftIdx = tryIdx;
            rightIdx = tryIdx;
            break;
        }

        assert(rightIdx >= leftIdx);
    }

    return m_timeToColMapping[leftIdx];
}

std::pair<int, int> SpectractorDialog::mapWavelengthToColSlow(const int wl) const
{
    if (m_wlToRowMapping.empty())
        throw std::runtime_error{"Empty mapping"};

    const auto first = m_wlToRowMapping.cbegin();
    const auto last = m_wlToRowMapping.crbegin();

    if (first->first > wl)
        throw std::runtime_error{"Wavelength out of range"};
    if (last->first < wl)
        throw std::runtime_error{"Wavelength out of range"};

    const int span = last->first - first->first;
    if (wl - first->first < span / 2) {
        for (const auto &it : m_wlToRowMapping) {
            if (it.first >= wl)
                return it;
        }
    } else {
        auto rit = m_wlToRowMapping.crbegin();
        while (rit != m_wlToRowMapping.crend()) {
            if (rit->first <= wl)
                return *rit;

            rit++;
        }
    }

    assert(false);
    throw std::runtime_error{"Wavelength not found"}; /* This should never happen */
}

void SpectractorDialog::onExportSlice()
{
    if (!haveData())
        return;

    if (!m_selectedSlice.valid)
        return;

    if (m_exportFileDlg->exec() != QDialog::Accepted)
        return;

    const auto sel = m_exportFileDlg->selectedFiles();
    if (sel.empty())
        return;

    if (!Exporter::exportSlice(sel[0], m_selectedSlice.data)) {
        QMessageBox mbox{QMessageBox::Warning, tr("Failed to write slice"), tr("Selected slice could not have been exported")};

        mbox.exec();
    }
}

void SpectractorDialog::onLoadSpectrumInteractive()
{
    assert(m_fileDlg != nullptr);

    if (m_fileDlg->exec() != QDialog::Accepted)
        return;

    const auto sel = m_fileDlg->selectedFiles();
    if (sel.empty())
        return;

    loadSpectrum(sel[0]);
}

void SpectractorDialog::onReferenceStateChanged()
{
    if (!haveData())
        return;

    auto reference = referenceWavelengthRow(m_sliceView->wlRef()->wavelength());

    m_selectedSlice = makeSlice(m_selectedSlice.row, m_selectedSlice.col,
                                m_selectedSlice.time, m_selectedSlice.wavelength,
				std::move(reference),
                                sliceViewBaseToBase(m_sliceView->base()));
    displaySlice(m_selectedSlice);
}

void SpectractorDialog::onRejectClose()
{
    if (m_mode == PROXY)
        m_selectedSlice = SelectedSlice{};

    reject();
}

void SpectractorDialog::onSliceBaseChanged(const SliceView::Base base)
{
    m_sliceView->wlRef()->setEnabled(base == SliceView::TIME);

    if (!haveData())
        return;

    m_selectedSlice = makeSlice(m_selectedSlice.row, m_selectedSlice.col,
                                m_selectedSlice.time, m_selectedSlice.wavelength, m_selectedSlice.reference,
                                sliceViewBaseToBase(base));
    displaySlice(m_selectedSlice);
}

void SpectractorDialog::onSpectrumPointSelected(const QPointF &posF)
{
    if (!haveData())
        return;

    const double t = posF.x();
    const int wl = int(std::floor(posF.y() * 1000.0 + 0.5));

    std::pair<int, int> row;
    const auto it = m_wlToRowMapping.find(wl);
    if (it == m_wlToRowMapping.cend()) {
        try {
            row = mapWavelengthToColSlow(wl);
        } catch (const std::runtime_error &) {
            return;
        }
    } else
        row = *it;

    std::pair<double, int> col;
    try {
        col = mapTimeToColumn(t);
    } catch (const std::runtime_error &) {
        return;
    }

    auto reference = referenceWavelengthRow(m_sliceView->wlRef()->wavelength());

    m_selectedSlice = makeSlice(row.second, col.second,
                                col.first, row.first, reference,
                                sliceViewBaseToBase(m_sliceView->base()));

    displaySlice(m_selectedSlice);
}

SpectractorDialog::SPGram SpectractorDialog::readSpectrum(const QString &path)
{
    LoadInProgressDialog dlg{};

    QEventLoop loop;
    QThread thr{};
    ReadWorker worker{path.toStdString()};

    worker.moveToThread(&thr);
    connect(&thr, &QThread::started, &dlg, &LoadInProgressDialog::show);
    connect(&thr, &QThread::started, &worker, &ReadWorker::process);
    connect(&worker, &ReadWorker::finished, &thr, &QThread::quit);
    connect(&worker, &ReadWorker::finished, &loop, &QEventLoop::quit);
    connect(&thr, &QThread::finished, &dlg, &LoadInProgressDialog::close);

    thr.start();
    loop.exec();
    thr.wait();

    if (!worker.ok)
        throw worker.ex;
    return worker.spgram;
}

std::pair<int, int> SpectractorDialog::referenceWavelengthRow(const double wavelength) const
{
    const int wl = int(std::floor(wavelength * 1000.0 + 0.5));

    if (!m_sliceView->wlRef()->referenceEnabled())
        return { -1, -1 };

    const auto it = m_wlToRowMapping.find(wl);
    if (it == m_wlToRowMapping.cend()) {
        try {
            return { mapWavelengthToColSlow(wl).second, wl };
        } catch (const std::runtime_error &) {
            return { -1, -1 };
        }
    } else
        return { it->second, wl };
}

SpectractorDialog::SelectedSlice::Base SpectractorDialog::sliceViewBaseToBase(const SliceView::Base base) noexcept
{
    switch (base) {
    case SliceView::TIME:
        return SelectedSlice::TIME;
    case SliceView::WAVELENGTH:
        return SelectedSlice::WAVELENGTH;
    }
    assert(false);
}

void SpectractorDialog::setupCtrlButtons(QDialogButtonBox *bbox, const Mode mode)
{
    connect(bbox, &QDialogButtonBox::rejected, this, &SpectractorDialog::onRejectClose);

    if (mode == PROXY) {
        bbox->addButton(QDialogButtonBox::Ok);
        connect(bbox, &QDialogButtonBox::accepted, this, &SpectractorDialog::accept);
    } else {
        auto b = new QPushButton{"Export slice"};
        bbox->addButton(b, QDialogButtonBox::AcceptRole);

        connect(b, &QPushButton::clicked, this, &SpectractorDialog::onExportSlice);
    }
}

void SpectractorDialog::setupWidgetCommon()
{
    setupCtrlButtons(ui->bbox_ctrls, m_mode);

    m_splitter = new QSplitter{Qt::Vertical, this};
    auto lay =  qobject_cast<QVBoxLayout *>(layout());
    assert(lay != nullptr);
    lay->insertWidget(0, m_splitter);

    m_spectrumView = new SpectrumView{};
    m_sliceView = new SliceView{};

    m_splitter->addWidget(m_spectrumView);
    m_splitter->addWidget(m_sliceView);

    auto menuBar = new QMenuBar{this};
    lay->insertWidget(0, menuBar);
    menuBar->addMenu(m_mainMenu);

    connect(m_spectrumView, &SpectrumView::pointSelected, this, &SpectractorDialog::onSpectrumPointSelected);
    connect(m_sliceView, &SliceView::baseChanged, this, &SpectractorDialog::onSliceBaseChanged);
    connect(m_sliceView->wlRef(), &ReferenceWavelengthWidget::stateChanged, this, &SpectractorDialog::onReferenceStateChanged);
}

ECHMET::Spectractor::Trace SpectractorDialog::trace()
{
    using Trace = ECHMET::Spectractor::Trace;

    if (m_mode == STANDALONE)
        return ECHMET::Spectractor::Trace{};

    if (!m_selectedSlice.valid)
        return ECHMET::Spectractor::Trace{};

    const auto base = m_selectedSlice.base == SelectedSlice::TIME ? Trace::TIME : Trace::WAVELENGTH;
    std::string xName = m_selectedSlice.base == SelectedSlice::TIME ? "time" : "wavelength";
    std::string xUnit = m_selectedSlice.base == SelectedSlice::TIME ? "min" : "nm";
    const double value = m_selectedSlice.base == SelectedSlice::TIME ? m_selectedSlice.wavelength : m_selectedSlice.time;

    Trace::SVec vec{};
    vec.reserve(size_t(m_selectedSlice.data.size()));
    for (const auto &pt : m_selectedSlice.data)
        vec.emplace_back(pt.x(), pt.y());

    auto absUnit = m_absUnit.toStdString();
    std::string yName{"Absorbance"};
    return Trace(base, value, m_selectedSlice.reference.second,
                 std::move(xName), std::move(xUnit), std::move(yName), std::move(absUnit),
                 std::move(vec));
}
