#include "loadinprogressdialog.h"
#include "ui_loadinprogressdialog.h"

LoadInProgressDialog::LoadInProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadInProgressDialog)
{
    ui->setupUi(this);
}

LoadInProgressDialog::~LoadInProgressDialog()
{
    delete ui;
}

void LoadInProgressDialog::reject()
{
    /* Do nothing - this prevents the user from closing the dialog */
}
