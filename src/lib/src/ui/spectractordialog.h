#ifndef SPECTRACTORDIALOG_H
#define SPECTRACTORDIALOG_H

#include "sliceview.h"

#include <map>
#include <utility>
#include <QMainWindow>
#include <spectractor.h>
#include <qwt_interval.h>

namespace Ui {
class SpectractorDialog;
}

class QFileDialog;
class QDialogButtonBox;
class QSplitter;
class SpectrumView;

class SpectractorDialog : public ECHMET::Spectractor::Dialog
{
  Q_OBJECT
public:
    enum Mode {
        PROXY,
        STANDALONE
    };

    using TimeColMap = std::vector<std::pair<double, int>>;
    using WlRowMap = std::map<int, int>;
    using SPGram = std::tuple<QwtInterval, QwtInterval, QwtInterval,
                              QVector<double>, int, QString,
                              TimeColMap, WlRowMap, uint32_t>;

    explicit SpectractorDialog(QWidget *parent = nullptr);
    explicit SpectractorDialog(const QString &path, QWidget *parent = nullptr);
    ~SpectractorDialog();

    ECHMET::Spectractor::Trace trace() override;

private:
    enum class IconID {
        LOAD,
        EXIT
    };

    class SelectedSlice {
    public:
        enum Base {
            TIME,
            WAVELENGTH
        };

        explicit SelectedSlice();
        SelectedSlice(const Base _base, const int _row, const int _col,
                      const double _time, const int _wavelength, std::pair<int, int> _reference,
                      const QwtInterval &_yInterval, QVector<QPointF> _data);
        SelectedSlice(const SelectedSlice &other) = default;
        SelectedSlice(SelectedSlice &&other) noexcept;

        SelectedSlice & operator=(SelectedSlice &&other) noexcept;

        Base base;
        int row;
        int col;
        double time;
        int wavelength;
	std::pair<int, int> reference;
        QwtInterval yInterval;
        QVector<QPointF> data;
        bool valid;
    };

    void displaySlice(const SelectedSlice &slice);
    QIcon getIcon(const IconID id) const;
    bool haveData() const;
    bool loadSpectrum(const QString &path);
    QMenu *makeMenuStandalone();
    QMenu *makeMenuProxy();
    SelectedSlice makeSlice(const int row, const int col, const double time, const int wavelength,
                            std::pair<int, int> reference, const SelectedSlice::Base base);
    QVector<QPointF> makeTimeSliceData(const int row, const int refRow);
    QVector<QPointF> makeWavelengthSliceData(const int col);
    std::pair<double, int> mapTimeToColumn(const double t) const;
    std::pair<int, int> mapWavelengthToColSlow(const int wl) const;
    std::pair<int, int> referenceWavelengthRow(const double wavelength) const;
    SPGram readSpectrum(const QString &path);
    void setupCtrlButtons(QDialogButtonBox *bbox, const Mode mode);
    void setupWidgetCommon();

    static SelectedSlice::Base sliceViewBaseToBase(const SliceView::Base base) noexcept;

    Ui::SpectractorDialog *ui;
    QSplitter *m_splitter;

    QMenu *m_mainMenu;
    QFileDialog *m_fileDlg;
    SpectrumView *m_spectrumView;
    SliceView *m_sliceView;

    QVector<double> m_spectrumMatrix;
    int m_spectrumMatrixCols;
    QwtInterval m_timeInterval;
    QwtInterval m_wlInterval;
    QwtInterval m_absInterval;
    QString m_absUnit;

    WlRowMap m_wlToRowMapping;
    TimeColMap m_timeToColMapping;

    SelectedSlice m_selectedSlice;
    QVector<QPointF> m_slice;

    QFileDialog *m_exportFileDlg;

    const Mode m_mode;

private slots:
    void onExportSlice();
    void onLoadSpectrumInteractive();
    void onReferenceStateChanged();
    void onRejectClose();
    void onSliceBaseChanged(const SliceView::Base base);
    void onSpectrumPointSelected(const QPointF &posF);
};

#endif // SPECTRACTORDIALOG_H
