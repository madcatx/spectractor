#include "referencewavelengthwidget.h"
#include "ui_referencewavelengthwidget.h"

ReferenceWavelengthWidget::ReferenceWavelengthWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReferenceWavelengthWidget)
{
    ui->setupUi(this);

    connect(ui->qcb_enabled, &QCheckBox::clicked, [this]() {
        emit this->stateChanged(referenceEnabled(), wavelength());
    });

    connect(ui->qdspb_refWl, static_cast<void (QDoubleSpinBox:: *)(double)>(&QDoubleSpinBox::valueChanged),
           [this]() {
               emit this->stateChanged(referenceEnabled(), wavelength());
    });
}

ReferenceWavelengthWidget::~ReferenceWavelengthWidget()
{
    delete ui;
}

bool ReferenceWavelengthWidget::referenceEnabled() const
{
    return ui->qcb_enabled->checkState() == Qt::Checked;
}

double ReferenceWavelengthWidget::wavelength() const
{
    return ui->qdspb_refWl->value();
}

void ReferenceWavelengthWidget::setRange(const double min, const double max, const double step)
{
    ui->qdspb_refWl->blockSignals(true);

    ui->qdspb_refWl->setRange(min, max);
    ui->qdspb_refWl->setSingleStep(step);
    ui->qdspb_refWl->setValue(min);

    ui->qdspb_refWl->blockSignals(false);
}
