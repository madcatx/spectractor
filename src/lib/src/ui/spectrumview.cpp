#include "spectrumview.h"
#include "ui_spectrumview.h"
#include "doubleclickableqwtplotzoomer.h"
#include "qwtspectrogrampicker.h"

#include <qwt_scale_draw.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_spectrogram.h>
#include <qwt_matrix_raster_data.h>
#include <qwt_color_map.h>
#include <QHBoxLayout>
#include <QFontMetrics>
#include <thread>

const QString SpectrumView::TITLE{"Spectrum view"};

static
int calcZScaleWidth(const QString &title, const QFontMetrics &fm)
{
    const int wm = fm.width(QStringLiteral("_"));
    const int wt = fm.width(title);

    return 10 * wm + wt;
}

static
QwtLinearColorMap * makeColorMap()
{
    auto map = new QwtLinearColorMap{};
    map->setColorInterval(QColor{2, 24, 93}, /* Dark black/blue */
                          QColor{255, 0, 0} /* Red */);
    map->addColorStop(0.5, QColor{72, 148, 14}); /* Green */
    map->addColorStop(0.75, QColor{220, 207, 17}); /* Yellow */

    return map;
}

static
QVector<double> makeZScaleData(const QwtInterval &interval)
{
    static const int STEPS{100};

    QVector<double> data{};
    data.reserve(STEPS);

    const double step = (interval.maxValue() - interval.minValue()) / STEPS;

    double v = interval.minValue();
    while (v <= interval.maxValue()) {
        data.append(v);
        v += step;
    }

    return data;
}

SpectrumView::SpectrumView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpectrumView)
{
    ui->setupUi(this);

    m_spectrumPlot= new QwtPlot{this};
    m_spectrumPlot->setAxisTitle(QwtPlot::yLeft, QString::fromUtf8("\xCE\xBB (nm)"));
    m_spectrumPlot->setAxisTitle(QwtPlot::xBottom, QStringLiteral("time (min)"));

    m_xhairHoriz = new QwtPlotCurve{};
    m_xhairVert = new QwtPlotCurve{};

    m_xhairHoriz->setPen(QColor{230, 230, 230});
    m_xhairVert->setPen(QColor{230, 230, 230});

    m_zoomer = new DoubleClickableQwtPlotZoomer{m_spectrumPlot->canvas()};
    m_zoomer->setTrackerMode(QwtPicker::AlwaysOff);
    m_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::LeftButton, Qt::ShiftModifier);

    m_picker = new QwtSpectrogramPicker(QwtPlot::Axis::xBottom, QwtPlot::Axis::yLeft,
                                        QwtPicker::NoRubberBand, QwtPicker::AlwaysOn,
                                        m_spectrumPlot->canvas());
    m_picker->setStateMachine(new QwtPickerClickPointMachine{}); /* Come blow my ClickPoint machine! */
    m_picker->setMousePattern(QwtEventPattern::MouseSelect1, Qt::RightButton);
    m_picker->setColor(QColor{230, 230, 230});

    m_rasterData = new QwtMatrixRasterData();
    m_rasterData->setResampleMode(QwtMatrixRasterData::BilinearInterpolation);
    m_picker->setRasterData(m_rasterData);

    m_colorMap = makeColorMap();

    m_spectrogram = new QwtPlotSpectrogram{};
    m_spectrogram->setDisplayMode(QwtPlotSpectrogram::ImageMode, true);
    m_spectrogram->setColorMap(m_colorMap);
    m_spectrogram->setData(m_rasterData);
    m_spectrogram->setRenderThreadCount(std::thread::hardware_concurrency());
    m_spectrogram->attach(m_spectrumPlot);
    m_xhairHoriz->attach(m_spectrumPlot);
    m_xhairVert->attach(m_spectrumPlot);

    m_zScalePlot = new QwtPlot{this};
    m_zScalePlot->setAxisTitle(QwtPlot::yRight, "Absorbance");
    m_zScalePlot->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    m_zScalePlot->enableAxis(QwtPlot::yRight, true);
    m_zScalePlot->enableAxis(QwtPlot::yLeft, false);
    m_zScalePlot->setAxisTitle(QwtPlot::xBottom, " ");
    m_zScalePlot->setAxisScale(QwtPlot::xBottom, 0, 1);
    m_zScalePlot->setAxisMaxMajor(QwtPlot::xBottom, 0);
    m_zScalePlot->setAxisMaxMinor(QwtPlot::xBottom, 0);

    m_zScaleData = new QwtMatrixRasterData{};
    m_zScaleData->setResampleMode(QwtMatrixRasterData::BilinearInterpolation);
    m_zScaleData->setInterval(Qt::XAxis, QwtInterval{0, 1});

    m_zScaleSpectrogram = new QwtPlotSpectrogram{};
    m_zScaleSpectrogram->setDisplayMode(QwtPlotSpectrogram::ImageMode, true);
    m_zScaleSpectrogram->setColorMap(m_colorMap);
    m_zScaleSpectrogram->setData(m_zScaleData);
    m_zScaleSpectrogram->setRenderThreadCount(std::thread::hardware_concurrency());
    m_zScaleSpectrogram->attach(m_zScalePlot);

    ui->ql_title->setText(TITLE);
    ui->ql_title->setFont(m_spectrumPlot->title().font());
    ui->ql_title->setStyleSheet(QStringLiteral("font-size: 14pt; font-weight: bold;"));

    ui->qhlay_spectrum->addWidget(m_spectrumPlot);
    ui->qhlay_spectrum->addWidget(m_zScalePlot);

    m_spectrumPlot->setMinimumHeight(128);
    m_zScalePlot->setMinimumHeight(128);
    {
        auto g = m_zScalePlot->geometry();
        const int w = calcZScaleWidth(QStringLiteral("Z-scale"), fontMetrics());
        g.setWidth(w + 2);
        m_zScalePlot->setGeometry(g);
        m_zScalePlot->setMinimumWidth(w);
        m_zScalePlot->setMaximumWidth(w);
    }

    connect(m_picker, static_cast<void (QwtSpectrogramPicker:: *)(const QPointF &)>(&QwtSpectrogramPicker::selected), this, &SpectrumView::onPointSelected);
}

SpectrumView::~SpectrumView()
{
    m_spectrogram->detach();
    m_zScaleSpectrogram->detach();

    delete ui;

    delete m_spectrogram;
}

void SpectrumView::onPointSelected(const QPointF &posF)
{
    emit pointSelected(posF);
}

void SpectrumView::setCrosshair(const double time, const double wavelength)
{
    QVector<QPointF> horiz{QPointF{m_rasterData->interval(Qt::XAxis).minValue() , wavelength},
                           QPointF{m_rasterData->interval(Qt::XAxis).maxValue(), wavelength}};
    QVector<QPointF> vert{QPointF{time, m_rasterData->interval(Qt::YAxis).minValue()},
                          QPointF{time, m_rasterData->interval(Qt::YAxis).maxValue()}};

    m_xhairHoriz->setSamples(horiz);
    m_xhairVert->setSamples(vert);

    m_spectrumPlot->replot();
}

void SpectrumView::setTitle(const QString &title)
{
    ui->ql_title->setText(QString{"%1 (%2)"}.arg(TITLE, title));
}

void SpectrumView::updateSpectrum(const QwtInterval &timeInterval, const QwtInterval &wlInterval, const QwtInterval &absInterval,
                                  const QVector<double> &data, const int columns, const QString &absUnits)
{
    m_rasterData->setInterval(Qt::XAxis, timeInterval);
    m_rasterData->setInterval(Qt::YAxis, wlInterval);
    m_rasterData->setInterval(Qt::ZAxis, absInterval);
    m_rasterData->setValueMatrix(data, columns);

    m_xhairHoriz->setSamples(QVector<QPointF>{});
    m_xhairVert->setSamples(QVector<QPointF>{});

    m_spectrumPlot->setAxisScale(QwtPlot::xBottom, timeInterval.minValue(), timeInterval.maxValue());
    m_spectrumPlot->setAxisScale(QwtPlot::yLeft, wlInterval.minValue(), wlInterval.maxValue());
    const auto brect = m_spectrogram->boundingRect();
    m_spectrumPlot->replot();
    m_zoomer->zoom(brect);
    m_zoomer->setZoomBase(brect);

    m_zScaleData->setValueMatrix(makeZScaleData(absInterval), 1);
    m_zScaleData->setInterval(Qt::YAxis, absInterval);
    m_zScaleData->setInterval(Qt::ZAxis, absInterval);
    m_zScalePlot->setAxisTitle(QwtPlot::yRight, QString{"Absorbance (%1)"}.arg(absUnits));
    m_zScalePlot->setAxisScale(QwtPlot::yLeft, absInterval.minValue(), absInterval.maxValue());
    m_zScalePlot->setAxisScale(QwtPlot::yRight, absInterval.minValue(), absInterval.maxValue());
    m_zScalePlot->replot();
}

