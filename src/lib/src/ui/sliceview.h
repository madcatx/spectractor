#ifndef SLICEVIEW_H
#define SLICEVIEW_H

#include <QWidget>
#include "referencewavelengthwidget.h"

namespace Ui {
class SliceView;
}

class DoubleClickableQwtPlotZoomer;
class QLabel;
class QwtInterval;
class QwtPlot;
class QwtPlotCurve;

class SliceView : public QWidget
{
    Q_OBJECT

public:
    enum Base {
        TIME,
        WAVELENGTH
    };

    explicit SliceView(QWidget *parent = nullptr);
    ~SliceView();

    Base base() const;
    void clear();
    void setPlotData(const QVector<QPointF> &data, const QString &xTitle, const QString &yTitle,
                     const QString &baseTitle);
    ReferenceWavelengthWidget * wlRef() const;

private:
    void setZoom();

    Ui::SliceView *ui;

    QwtPlot *m_plot;
    QwtPlotCurve *m_plotCurve;
    DoubleClickableQwtPlotZoomer *m_zoomer;
    QLabel *m_title;

    static const QString TITLE;

private slots:
    void onBaseChanged();

signals:
    void baseChanged(const Base mode);
};

Q_DECLARE_METATYPE(SliceView::Base)

#endif // SLICEVIEW_H
