#include "sliceview.h"
#include "ui_sliceview.h"
#include "doubleclickableqwtplotzoomer.h"

#include <QLabel>
#include <QVBoxLayout>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <cassert>

const QString SliceView::TITLE{"Slice view"};

SliceView::SliceView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SliceView)
{
    ui->setupUi(this);

    auto lay = qobject_cast<QVBoxLayout *>(layout());
    assert(lay != nullptr);

    m_title = new QLabel{TITLE, this};

    m_plot = new QwtPlot{this};
    m_plot->setCanvasBackground(Qt::white);

    m_zoomer = new DoubleClickableQwtPlotZoomer{m_plot->canvas()};
    m_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
    m_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::LeftButton, Qt::ShiftModifier);

    m_plotCurve = new QwtPlotCurve{};
    m_plotCurve->attach(m_plot);

    m_title->setText(TITLE);
    m_title->setFont(m_plot->title().font());
    m_title->setStyleSheet(QStringLiteral("font-size: 14pt; font-weight: bold;"));
    m_title->setAlignment(Qt::AlignHCenter);

    lay->insertWidget(0, m_plot);
    lay->insertWidget(0, m_title);

    m_plot->setMinimumHeight(128);

    ui->qcbox_sliceBy->addItem(tr("Time"), TIME);
    ui->qcbox_sliceBy->addItem(tr("Wavelength"), WAVELENGTH);

    qRegisterMetaType<Base>("Base");

    connect(ui->qcbox_sliceBy, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &SliceView::onBaseChanged);
}

SliceView::~SliceView()
{
    delete ui;
}

SliceView::Base SliceView::base() const
{
    return ui->qcbox_sliceBy->currentData().value<Base>();
}

void SliceView::clear()
{
    m_plotCurve->setSamples(QVector<QPointF>{});
    m_plot->replot();
    setZoom();
}

void SliceView::onBaseChanged()
{
    emit baseChanged(base());
}

void SliceView::setPlotData(const QVector<QPointF> &data, const QString &xTitle, const QString &yTitle,
                            const QString &baseTitle)
{
    m_title->setText(QString{"%1 - %2"}.arg(TITLE, baseTitle));
    m_plotCurve->setSamples(QVector<QPointF>{});
    setZoom();

    m_plotCurve->setSamples(data);
    m_plot->setAxisTitle(QwtPlot::xBottom, xTitle);
    m_plot->setAxisTitle(QwtPlot::yLeft, yTitle);

    m_plot->replot();

    setZoom();
}

void SliceView::setZoom()
{
    const auto brect = m_plotCurve->boundingRect();
    m_zoomer->zoom(brect);
    m_zoomer->setZoomBase(brect);
}

ReferenceWavelengthWidget * SliceView::wlRef() const
{
    return ui->qw_wlRef;
}
