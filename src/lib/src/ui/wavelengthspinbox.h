#ifndef WAVELENGTHSPINBOX_H
#define WAVELENGTHSPINBOX_H

#include <QDoubleSpinBox>

class WavelengthSpinBox : public QDoubleSpinBox {
    Q_OBJECT
public:
    WavelengthSpinBox(QWidget *parent = nullptr);

protected:
    QValidator::State validate(QString &input, int &pos) const;

};

#endif // WAVELENGTHSPINBOX_H
