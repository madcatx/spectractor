#include "wavelengthspinbox.h"

#include <QLocale>

WavelengthSpinBox::WavelengthSpinBox(QWidget *parent) :
    QDoubleSpinBox{parent}
{
    setPrefix("\xCE\xBB = ");
    setSuffix(" nm");
    setDecimals(1);
}

QValidator::State WavelengthSpinBox::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos);

    const int min = int(minimum() * 1000);
    const int max = int(maximum() * 1000);
    const int step = int(singleStep() * 1000);

    const auto numInput = QString{input}.replace(prefix(), "").replace(suffix(), "");

    bool ok;
    const int val = int(QLocale::system().toDouble(numInput, &ok) * 1000);
    if (!ok)
        return QValidator::Intermediate;

    if (val < min || val > max)
        return QValidator::Intermediate;

    if ((val - min) % step != 0)
        return QValidator::Intermediate;

    return QValidator::Acceptable;
}
