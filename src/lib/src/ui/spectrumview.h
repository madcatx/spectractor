#ifndef SPECTRUMVIEW_H
#define SPECTRUMVIEW_H

#include <QWidget>
#include <qwt_interval.h>

namespace Ui {
class SpectrumView;
}

class QwtPlot;
class QwtPlotCurve;
class QwtPlotSpectrogram;
class QwtMatrixRasterData;
class QwtLinearColorMap;
class QwtSpectrogramPicker;
class DoubleClickableQwtPlotZoomer;

class SpectrumView : public QWidget
{
    Q_OBJECT
public:
    explicit SpectrumView(QWidget *parent = nullptr);
    ~SpectrumView();

    void setCrosshair(const double time, const double wavelength);
    void setTitle(const QString &title);
    void updateSpectrum(const QwtInterval &xInterval, const QwtInterval &yInterval, const QwtInterval &zInterval,
                        const QVector<double> &data, const int columns, const QString &absUnits);

private:
    Ui::SpectrumView *ui;

    QwtLinearColorMap *m_colorMap;
    QwtPlotSpectrogram *m_spectrogram;
    QwtMatrixRasterData *m_rasterData;
    QwtPlot *m_spectrumPlot;
    DoubleClickableQwtPlotZoomer *m_zoomer;
    QwtSpectrogramPicker *m_picker;

    QwtPlotCurve *m_xhairHoriz;
    QwtPlotCurve *m_xhairVert;

    QwtMatrixRasterData *m_zScaleData;
    QwtPlotSpectrogram *m_zScaleSpectrogram;
    QwtPlot *m_zScalePlot;

    static const QString TITLE;

signals:
    void pointSelected(const QPointF &point);

private slots:
    void onPointSelected(const QPointF &posF);

};

#endif // SPECTRUMVIEW_H
