#include "qwtspectrogrampicker.h"

#include <qwt_plot_canvas.h>
#include <qwt_raster_data.h>
#include <cmath>
#include <QFileDialog>

static
QChar fieldSep(const QChar &sep)
{
    if (sep == QChar{'.'})
        return ',';
    return ';';
}

QwtSpectrogramPicker::QwtSpectrogramPicker(QWidget *canvas) :
    QwtPlotPicker{canvas},
    m_data{nullptr},
    m_loc{QLocale::system()},
    m_sep{fieldSep(m_loc.decimalPoint())},
    m_color{Qt::black}
{}

QwtSpectrogramPicker::QwtSpectrogramPicker(int xAxis, int yAxis, RubberBand rubberBand, DisplayMode trackerMode, QWidget *canvas) :
    QwtPlotPicker{xAxis, yAxis, rubberBand, trackerMode, canvas},
    m_data{nullptr},
    m_loc{QLocale::system()},
    m_sep{fieldSep(m_loc.decimalPoint())},
    m_color{Qt::black}
{}

void QwtSpectrogramPicker::setColor(const QColor &color)
{
    m_color = color;
}

void QwtSpectrogramPicker::setRasterData(const QwtRasterData *data)
{
    m_data = data;
}

QwtText QwtSpectrogramPicker::trackerText(const QPoint &pos) const
{
    return trackerTextF(invTransform(pos));
}

QwtText QwtSpectrogramPicker::trackerTextF(const QPointF &posF) const
{
    if (m_data == nullptr)
        return QwtText{};

    const double x = posF.x();
    const double y = posF.y();
    const double z = m_data->value(x, y);

    if (std::isnan(z))
        return QwtText{};

    auto text = QwtText{QString{"t: %2%1 \xCE\xBB: %3%1 A: %4"}
                        .arg(m_sep, m_loc.toString(x, 'f', 4),
                             m_loc.toString(y, 'f', 1),
                             m_loc.toString(z, 'f', 5))};

    text.setColor(m_color);

    return text;
}
