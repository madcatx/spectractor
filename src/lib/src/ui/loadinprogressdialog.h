#ifndef LOADINPROGRESSDIALOG_H
#define LOADINPROGRESSDIALOG_H

#include <QDialog>

namespace Ui {
class LoadInProgressDialog;
}

class LoadInProgressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoadInProgressDialog(QWidget *parent = nullptr);
    ~LoadInProgressDialog() override;
    void reject() override;

private:
    Ui::LoadInProgressDialog *ui;
};

#endif // LOADINPROGRESSDIALOG_H
