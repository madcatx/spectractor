#include "exporter.h"

#include <QFile>
#include <QTextStream>

bool Exporter::exportSlice(const QString &path, const QVector<QPointF> &data)
{
    QFile fh{path};
    if (!fh.open(QIODevice::WriteOnly))
        return false;

    QTextStream str{&fh};

    for (const auto &pt : data)
        str << pt.x() << ";" << pt.y() << "\n";

    return true;
}
