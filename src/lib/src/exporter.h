#ifndef EXPORTER_H
#define EXPORTER_H

#include <QPointF>
#include <QVector>

class Exporter
{
public:
    Exporter() = delete;

    static bool exportSlice(const QString &path, const QVector<QPointF> &data);
};

#endif // EXPORTER_H
