#include "ui/spectractordialog.h"

namespace ECHMET {

Spectractor::Trace::Trace() :
    base{TIME},
    value{0},
    reference{0},
    xName{""}, xUnit{""},
    yName{""}, yUnit{""},
    data{SVec{}},
    valid{false}
{}

Spectractor::Spectractor::Trace::Trace(const Base _base, const double _value, const double _reference,
                                       std::string _xName, std::string _xUnit,
                                       std::string _yName,  std::string _yUnit,
                                       SVec _data) noexcept :
    base{_base},
    value{_value},
    reference{_reference},
    xName(std::move(_xName)), xUnit(std::move(_xUnit)),
    yName(std::move(_yName)), yUnit(std::move(_yUnit)),
    data(std::move(_data)),
    valid{true}
{}

Spectractor::Spectractor::Trace::Trace(Trace &&other) noexcept :
    base{other.base},
    value{other.value},
    reference{other.reference},
    xName(std::move(other.xName)), xUnit(std::move(other.xUnit)),
    yName(std::move(other.yName)), yUnit(std::move(other.yUnit)),
    data(std::move(other.data)),
    valid(other.valid)
{}

Spectractor::Dialog::Dialog(QWidget *parent) :
    QDialog{parent}
{}

Spectractor::Dialog * Spectractor::proxyDialog(const std::string &path)
{
    return new SpectractorDialog(QString::fromStdString(path));
}

Spectractor::Dialog * Spectractor::standaloneDialog()
{
    return new SpectractorDialog();
}

}
